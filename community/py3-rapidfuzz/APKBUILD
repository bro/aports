# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-rapidfuzz
pkgver=3.5.1
pkgrel=0
pkgdesc="Rapid fuzzy string matching in Python using various string metrics"
url="https://github.com/maxbachmann/RapidFuzz"
arch="all"
license="MIT"
makedepends="
	cmake
	cython
	py3-gpep517
	py3-rapidfuzz-capi
	py3-scikit-build
	py3-setuptools
	python3-dev
	samurai
	"
checkdepends="
	py3-hypothesis
	py3-numpy
	pytest
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/r/rapidfuzz/rapidfuzz-$pkgver.tar.gz"
builddir="$srcdir/rapidfuzz-$pkgver"

case "$CARCH" in
x86*)
	# float rounding
	options="$options !check"
	;;
esac

build() {
	RAPIDFUZZ_BUILD_EXTENSION=1 \
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2 -U_FORTIFY_SOURCE" \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer \
		.dist/rapidfuzz*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/rapidfuzz*.whl
}

sha512sums="
346f02c69995d250e023475eccc6da8ed99ab837a209d1f5f7c30f22cb42e883dec9ec7cc7d1b7c89176c976cc9a0db00eb5658b5dfb0b6088f02f9686c895c1  rapidfuzz-3.5.1.tar.gz
"
